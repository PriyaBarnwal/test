import flowai from './themes/flowai'
import khoros from './themes/khoros'
import khorosv2 from './themes/khoros-v2'

let theme

switch(window.__flowai__app_theme) {
  case 'khoros': {
    theme = khoros
    break
  }
  case 'khoros-v2': {
    theme = khorosv2
    break
  }
  default: {
    theme = flowai
    break
  }
}

const createThemeItemName = keys => keys.join('-').split(/(?=[A-Z])/).join('-').toLocaleLowerCase()

const createThemeLines = (theme, lines = [], parentKeys = []) => {
  if(!theme) {
    return ''
  }

  const themeKeys = Object.keys(theme) 
  for (let i = 0; i < themeKeys.length; i++) {
    const key = themeKeys[i]
    const item = theme[key]

    if(typeof item === 'string') {
      lines.push(`--${createThemeItemName([ ...parentKeys, key ])}: ${item};`)
      continue
    }
    
    if(Array.isArray(item)) {
      for (let x = 0; x < item.length; x++) {
        const child = item[x]
        createThemeLines(child, lines,  [ ...parentKeys, key ])
      }
    } else if(typeof item !== 'string') {
      createThemeLines(item, lines, [ ...parentKeys, key ])
    }
  }
  return lines
}
console.info('theme', createThemeLines(theme))
document.write(`
<style>
:root {
  --channel-specific: ${theme.colors.tint.design};

  ${createThemeLines(theme).join('\r\n')}
}
</style>
`)

export default theme