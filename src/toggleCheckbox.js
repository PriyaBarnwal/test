import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, css } from 'aphrodite'
import Theme from './theme'

const styles = StyleSheet.create({
  toggleCheckbox: {
    position: 'relative',
    width: '40px',
    WebkitUserSelect: 'none',
    MozUserSelect: 'none',
    MsUserSelect: 'none'
  },
  toggleCheckboxCheckbox: {
    display: 'none'
  },
  toggleCheckboxLabel: {
    display: 'block',
    overflow: 'hidden',
    cursor: 'pointer',
    borderRadius: '20px'
  },
  toggleCheckboxInner: {
    display: 'block',
    width: '200%',
    marginLeft: '-100%',
    transition: 'margin 0.3s ease-in 0s',
    ':before': {
      display: 'block',
      float: 'left',
      width: '50%',
      height: '24px',
      padding: '1px 0 0 0',
      lineHeight: '24px',
      fontSize: '11px',
      fontFamily: 'Gilroy-Bold, sans-serif',
      boxSizing: 'border-box',
      content: '""',
      paddingLeft: '8px',
      backgroundColor: Theme.colors.tint.c0,
      color: Theme.colors.text.c0
    },
    ':after': {
      display: 'block',
      float: 'left',
      width: '50%',
      height: '24px',
      padding: '1px 0 0 0',
      lineHeight: '24px',
      fontSize: '11px',
      fontFamily: 'Gilroy-Medium, sans-serif',
      boxSizing: 'border-box',
      content: '""',
      paddingRight: '8px',
      backgroundColor: Theme.colors.fill.c0,
      color: Theme.colors.text.c2,
      textAlign: 'right'
    }
  },
  toggleCheckboxInnerOn: {
    marginLeft: '0'
  },
  toggleCheckboxSwitch: {
    display: 'block',
    width: '16px',
    margin: '4px',
    background: Theme.colors.text.c2,
    position: 'absolute',
    top: '0',
    bottom: '0',
    right: '17px',
    borderRadius: '40px',
    transition: 'all 0.3s ease-in 0s'
  },
  toggleCheckboxSwitchOn: {
    right: 0,
    background: Theme.colors.text.c0
  }
})

const ToggleCheckbox = props => {
  const {
    checked,
    disabled,
    isLoading,
    onChange
  } = props

  return (
    <div className={css(styles.toggleCheckbox)}>
      <label className={css(styles.toggleCheckboxLabel)}>
        <input
          type="checkbox"
          className={css(styles.toggleCheckboxCheckbox)}
          checked={checked}
          onChange={onChange}
          disabled={isLoading || disabled}
        />
        <span className={css(styles.toggleCheckboxInner, checked && styles.toggleCheckboxInnerOn)} />
        <span className={css(styles.toggleCheckboxSwitch, checked && styles.toggleCheckboxSwitchOn)} />
      </label>
    </div>
  )
};

ToggleCheckbox.defaultProps = {
  isLoading: false,
  checked: false,
  disabled: false
}

ToggleCheckbox.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
  checked: PropTypes.bool.isRequired,
  disabled: PropTypes.bool.isRequired
}

export default ToggleCheckbox
