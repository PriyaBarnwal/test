import React from "react";
import ReactDOM from "react-dom";
import ToggleCheckbox from "./toggleCheckbox";

import "./index.css";

const App = () => (
  <div className="container">
    <div>Name: test</div>
    <ToggleCheckbox onChange={()=> console.log('clicked')}/>
  </div>
);
ReactDOM.render(<App />, document.getElementById("app"));